@echo off
rem Compiling script

rem Creating target directory structure
rmdir /s /q target
mkdir target
cd target
mkdir js
mkdir css
cd..

rem Compiling JavaScript
break > target\js\main.js
type src\js\components\aside.js >> target\js\main.js
type src\js\components\expandable.js >> target\js\main.js
type src\js\components\select.js >> target\js\main.js
type src\js\components\progress_bar.js >> target\js\main.js
type src\js\spa.js >> target\js\main.js
type src\js\main.js >> target\js\main.js

rem Compiling CSS
sass --watch src/main.scss:target/css/main.css
