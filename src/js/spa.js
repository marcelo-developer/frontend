var spa = {
	globalTargetSelector: $('meta[name="spa-target"]').attr('content'),
	globalProgressIndicatorSelector: $('meta[name="spa-progress"]').attr('content'),
	validLinks: [],

	bindEvents: function() {
		spa.validate();
		$(spa.validLinks).click(spa.beginRequest);
	},

	validate: function() {

		$('a:not([target^="_"])').each(function(index) {
			var target = spa.findTarget($(this).attr('target'));

			if (!target)
				console.log('[Warning] No target found to render response of "' + $(this).prop('href') + '". You should define spa-target meta tag if you want to define a global target for the SPA system or "href" attribute in the anchor tag. Consult documentation for further information.');
			else
				spa.validLinks.push(this);
		});
	},

	beginRequest: function(event) {
		var link = $(event.target);
		var url = link.prop('href');
		var target = spa.findTarget(link.attr('target'));
		var progressIndicator = spa.findProgressIndicator(link.attr('data-progress'));

		if (progressIndicator)
			progressIndicator.addClass('show');

		$.get({
			url: url,
			cache: false,
			success: spa.renderResponse(target, url),
			error: spa.renderError(target, url),
			complete: spa.completeRequest(progressIndicator)
		});

		link.parent().click();
		return false;
	},

	renderResponse: function(target, url) {
		return function(data) {
			var downloadedPage = $('<html>').html(data);
			downloadedPage
				.find('script')
				.each(function(index) {
					var pageUrl = url.substr(0, url.lastIndexOf('/') + 1);
					var scriptRelativeUrl = $(this).attr('src');
					$.getScript(pageUrl + scriptRelativeUrl);
				})
				.remove();
			downloadedPage
				.find('link')
				.each(function(index, element) {
					$(element).attr('href', url.substr(0, url.lastIndexOf('/') + 1) + $(element).attr('href'));
				});
			target.html(downloadedPage.html());
		};
	},

	renderError: function(target, url) {
		return function(xhr) {
			target.empty().append(
				$('<h1>').html(xhr.statusText),
				$('<h2>').html('The request to "' + url + '" was unsuccessful due to the following error:'),
				$('<p>').html('Error ' + xhr.status + ' - ' + xhr.statusText));
		}
	},

	completeRequest: function(progressIndicator) {

		if (progressIndicator)
			progressIndicator.removeClass('show');
	},

	findTarget(targetSelector) {
		var target = $(targetSelector || spa.globalTargetSelector);

		if ($(target).length === 0)
			return undefined;

		return target;
	},

	findProgressIndicator(indicatorSelector) {
		var indicator = $(indicatorSelector || spa.globalProgressIndicatorSelector);

		if ($(indicator).length === 0)
			return undefined;

		return indicator;
	}
}
/* Necessary new line. Removing might lead to compilation errors. */