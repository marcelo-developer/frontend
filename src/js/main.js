// Depends on: components/aside.js
// Depends on: components/expandable.js
// Depends on: components/select.js
// Depends on: components/progress_bar.js
// Depends on: spa.js

$(function() {
	// HTML is loaded
	menuItemEvents();
	selectEvents();
	expandableEvents();
	progressBar.initialize();
	spa.bindEvents();
	aside.initialize();
});

$(window).on('load', function() {
    // Everything is loaded
	scrollEvents();
});

function scrollEvents() {
	var lowerSection = getLastMenu();
	var documentBottom = $(document).height();
	$(window).scroll(function() {
		scrollClasses();
		scrollSections(documentBottom, lowerSection);
	});
	$(window).scroll();
}

function scrollClasses() {
	var remove = 'data-noscroll';
	var add = 'data-scroll';

	if ($(window).scrollTop() < $(window).height()) {
		var remove = 'data-scroll';
		var add = 'data-noscroll';
	}

	$('[' + remove + ']').each(function(index, element) {
		$(element).removeClass($(element).attr(remove));
	});

	$('[' + add + ']').each(function(index, element) {
		$(element).addClass($(element).attr(add));
	});
}

function scrollSections(documentBottom, lastMenu) {
	var screenTop = $(window).scrollTop();
	var screenBottom = screenTop + $(window).height();
	var selected = null;

	if (screenBottom === documentBottom)
		selected = lastMenu;
	else {

		for (menuItem of $('li[data-section]')) {
			var section = $('#' + $(menuItem).attr('data-section'));
			var sectionTop = $(section).offset().top;
			var sectionBottom = sectionTop + $(section).outerHeight(true);

			if (((sectionTop - 1) <= screenTop) && ((sectionBottom + 1) >= screenTop))
				selected = menuItem;
		};
	}

	if (selected !== null) {

		$('[data-section]').each(function(index, menuItem) {

			if ($(menuItem).attr('data-section') == $(selected).attr('data-section'))
				$(menuItem).addClass('selected');
			else
				$(menuItem).removeClass('selected');
		});
	}
}

function getLastMenu() {
	var lowerSection = null;

	for (menuItem of $('li[data-section]')) {
		var section = $('#' + $(menuItem).attr('data-section'));
		var sectionTop = $(section).offset().top;

		if ((lowerSection === null) || (sectionTop > $(lowerSection).offset().top))
			lowerSection = section;
	}

	return $('li[data-section=\'' + $(lowerSection).attr('id') + '\']');
}

function menuItemEvents() {
	$('li[data-section], a[data-section]').click(function() {
		$(window).off('scroll');
		$('html, body').animate({scrollTop: $('#' + $(this).attr('data-section')).offset().top}, 500, function() {
			scrollEvents();
		});
	});
}
