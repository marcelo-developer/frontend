function selectEvents() {
	$('body').click(hideOptions);
	$('select').each(function() {
		var selectName = $(this).attr('name');
		var selectClasses = $(this).attr('class');
		var select = $('<div></div>')
				.addClass('select-wrapper')
				.addClass(selectClasses)
				.attr('id', 'select_' + selectName)
				.click(selectName, showOptions);
		var options = $('<ul></ul>')
				.addClass('options-wrapper removed')
				.addClass(selectClasses)
				.attr('id', 'options_' + selectName);

		$(this).children().each(function() {
			options.append($('<li></li>')
				.html($(this).html())
				.attr('data-value', $(this).val())
				.click(selectName, selectOption));

			if ($(this).attr('selected')) {
				select.append(
					$('<span>'+ $(this).html() +'</span>'),
					$('<i></i>')
						.addClass('material-icons inline')
						.html('arrow_drop_down'));
			}
		});

		$(select).insertAfter(this)
		$(options).insertAfter(select);
	});
}

function place(element) {
	//$('div.options-wrapper').height
}

function showOptions(event) {
	$('ul#options_' + event.data).show();
	event.originalEvent.stopPropagation();
}

function hideOptions() {
	$('ul.options-wrapper').hide();
}

function selectOption(event) {
	$('div#select_' + event.data + ' > span').html($(event.target).html());
	$('select[name="' + event.data + '"]').val($(event.target).attr('data-value')).change();
}
/* Necessary new line. Removing might lead to compilation errors. */