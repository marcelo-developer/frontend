var aside = {

	initialize: function() {

		$(window).resize(() => {
			var classes = [
				'.layout-container.hide-left',
				'.layout-container.hide-right',
				'.layout-container.show-left',
				'.layout-container.show-right'
			];
			$(classes.join(',')).each((index, container) => {
				var leftSide = $('<aside>', {class: 'left'});
				var rightSide = $('<aside>', {class: 'right'});
				var containerVirtual = $('<div>', aside.asideAttrs(container))
						.append(leftSide)
						.append(rightSide);
				$('body').append(containerVirtual);
				$(container).removeClass(aside.hidden(leftSide) ? 'hide-left' : 'show-left');
				$(container).removeClass(aside.hidden(rightSide) ? 'hide-right' : 'show-right');
				$(containerVirtual).remove();
			});
		});

		$('aside[data-toggler], .aside[data-toggler]').each((index, side) => {
			var side = $(side);
			var container = side.closest('.layout-container');
			var suffix = side.hasClass('left') ? '-left' : (side.hasClass('right') ? '-right' : '');

			$(side.attr('data-toggler')).click((event) => {
				var showClass = 'show' + suffix;
				var hideClass = 'hide' + suffix;

				if (container.hasClass(showClass))
					container.removeClass(showClass);
				else if (container.hasClass(hideClass))
					container.removeClass(hideClass);
				else
					container.addClass(aside.hidden(side) ? showClass : hideClass);

				event.stopPropagation();
				$(window).one('click', () => {
					container.removeClass(showClass);
				});
			});

		});
	},

	asideAttrs: function(container, suffix) {
		var classes = $(container)
				.attr('class')
				.split(/\s+/)
				.filter(clazz => {
					return (clazz.indexOf('aside') !== -1);
				})
				.concat('layout-container')
				.join(' ');
		return {
			class: classes,
			style: 'transform: translateY(-100%)'
		}
	},

	hidden: function(side) {
		return (parseInt(side.css('transform').split(',')[4]) !== 0);
	}
}
/* Necessary new line. Removing might lead to compilation errors. */