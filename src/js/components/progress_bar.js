var progressBars = {};
var progressBar = {

	initialize: function() {
		progressBars = {};

		$('[data-progress]').each(function() {
			var bar = $(this);
			progressBars[bar.attr('id')] = {

				update: function(progress) {
					progress = Number(progress);

					if (Number.isNaN(progress) || (progress > 1) || (progress < 0))
						progress = 1;

					bar.attr('data-progress', progress);
					bar.css('border-left-width', bar.outerWidth() * progress)
				}
			};
			progressBars[bar.attr('id')].update(bar.attr('data-progress'));
		});
	}
}
/* Necessary new line. Removing might lead to compilation errors. */