function expandableEvents() {
	$('.expandable').click(function(event) {
		$(event.target)
			.toggleClass('expanded')
			.next()
			.slideToggle(400);
	});
}
/* Necessary new line. Removing might lead to compilation errors. */