$(function() {
	progressBar.initialize();
	$('button#pageProgress').click(function(event) {
		$('div.progress#progress').toggleClass('show');

		if ($('div.progress#progress').hasClass('show'))
			$(event.target).html('Hide progress bar');
		else
			$(event.target).html('Show progress bar');
	});
	$('#progressValue').change(function() {
		progressBars.determinateProgress.update($(this).val() / 100);
	});
});