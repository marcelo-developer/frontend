# Static Server Frontend

## Prerequisites

#### Dart Sass

This project is developed with that preprocessor and so compatibility with other Sass implementations are not guaranteed.

In order to building scripts to work properly, Sass preprocessor must be installed globally. You can do that easily with npm, typing the following command on a machine with npm installed:

####

```
npm install --global sass
```

#### A static server, like Apache

The single page application feature will only work with a running static server, as many modern browsers has issues related to CORS requests to local machine without a server, even if files are located in the same machine.



## How to build

This project has scripts to automate build in both Linux and Windows.

To build on Linux, type ```./comp``` on a terminal inside the directory where you have cloned the project.

On Windows use ```comp.bat``` the same way.